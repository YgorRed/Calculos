
package raizes;


public class RaizQuadrada {
    private double n1;

    public RaizQuadrada() {
    }

    public RaizQuadrada(double n1) {
        this.n1 = n1;
    }
    
    

    public double getN1() {
        return n1;
    }

    public void setN1(double n1) {
        this.n1 = n1;
    }
    public double raiz(){
    return  Math.sqrt(n1);
    
    }
}
