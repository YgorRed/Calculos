
package raizes;


public class RaizCubica {
    private double n1 ;

    public RaizCubica() {
    }

    public RaizCubica(double n1) {
        this.n1 = n1;
    }
    

    public double getN1() {
        return n1;
    }

    public void setN1(double n1) {
        this.n1 = n1;
    }
    
    
    public double rizcubica(){
    return Math.cbrt(n1);
    }
}
