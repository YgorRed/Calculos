
package calculos.angulo;

public class Cosseno {

    private double n1;

    public Cosseno() {
    }

    public Cosseno(double n1) {
        this.n1 = n1;
    }
    

    public double getN1() {
        return n1;
    }

    public void setN1(double n1) {
        this.n1 = n1;
    }
    
    public double cosseno(){
      return Math.cos(n1);
   
    }    
}
