
package calculos.angulo;


public class GrausRadianus {
  private static  double n1 = 10 ;  

    public GrausRadianus() {
    }

  
    public GrausRadianus(double n1) {
        this.n1 = n1;
    }


    public double getN1() {
        return n1;
    }

    public void setN1(double n1) {
        this.n1 = n1;
    }
    public static double Radianos(double n1){
    
return Math.toRadians(n1);
    }
    
}
